1. What is an alternative term for pinging?
```
> echo
```

2. What is the term used when a server cannot be pinged?
```
> Request Timed Out
```

3. A security setting that limits the communication of systems from outside the server.
```
> Outbound Security Rules
```

4. A security setting that limits the communication of systems from within the server.
```
> Inbound Security Rules
```

5. From which tab in the instance information summary can we change the firewall rules of the instance?
```
> Networking Tab
```

6. ICMP stands for?
```
> Internet Control Message Protocol
```

7. TCP stands for?
```
> Transmission Control Protocol
```

8. Which protocol does a ping request use?
```
> Internet Control Message Protocol 
```

9. What is the IP address that indicates access from anywhere?
```
> 0.0.0.0
```

10. A command that can be used to look on various usage metrics for a given server.
```
> htop
```

11. Processor or memory __________ means the amount of resources needed to keep a given task or process running smoothly within the operating system.
```
> Utilization
```

12. A command that can be used to determine disk storage usage.
```
> df
```

13. From which tab in the instance information summary can we view the usage of an instance without directly accessing the instance?
```
> Monitoring
```

14. A command that can be used to edit the contents of a file.
```
> nano
```

15. A command used to host a Node.js app.
```
> npx serve
```

16. Which protocol is used to enable access to a Node.js app on a given port?
```
> Transmission Control Protocol 
```